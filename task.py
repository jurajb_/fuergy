from datetime import datetime, timezone, timedelta
from croniter import croniter
import threading

def dates_diff(d1, d2):
    return abs(d2-d1).seconds

def execute(job, timer):
    t = threading.Timer(timer, execute, [job,timer])
    t.start()
    print("Executed: ", jobs[job], datetime.now(timezone.utc))

crons = ["*/2 * * * * ", "*/10 * * * * ", "* * * * 6 "]

jobs = {
    "job1": crons[0],
    "job2": crons[1],
    "job3": crons[2],
}

for key,val in jobs.items():
    curr_dt = datetime.now(timezone.utc)
    iter_dt = croniter(val,curr_dt)
    timer_dt = dates_diff(iter_dt.get_next(datetime),iter_dt.get_next(datetime))
    t = threading.Timer(0, execute, [key, timer_dt])
    t.start()


